angular.module("NewsApp")
    .component("newsAppLogin", {  
        templateUrl: "/app/components/login/login.html",
    
        controller: function($state, UserService) {
            var ctrl = this;
            
            ctrl.username = "tomasz";
            ctrl.password = null;
            
            ctrl.cancel = function() {
                $state.go("home");
            };
            
            ctrl.ok = function() {
                UserService
                    .logIn(ctrl.username, ctrl.password)
                    .then(function() {
                        if (UserService.loggedIn)
                            $state.go("home");
                    });
            };
        }
    
    });


