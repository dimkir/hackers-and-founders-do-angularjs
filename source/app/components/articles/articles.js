angular.module("NewsApp")
    .component("newsAppArticles", {  
        templateUrl: "/app/components/articles/articles.html",
    
        controller: function(ArticlesService, $interval) {
            var ctrl = this;   
            
            // expose the service to databindings in HTML 
            ctrl.ArticlesService = ArticlesService;
            
            ctrl.count = function() {
                return ArticlesService.items.length; 
            };
            
            if (!ArticlesService.populated)
                ArticlesService.load();
            
        }
    
    });


