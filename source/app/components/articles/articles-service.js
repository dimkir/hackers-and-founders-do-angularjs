angular
    .module("NewsApp")
    .factory("ArticlesService", function($http, $log, $timeout) {
    
        var service = {
            
            populating: false,
            populated: false,
            items: [],
            
            load: function() {
                service.populating = true;
                $timeout(function() {                    
                    $http
                        .get("app/data/articles.json", { cache: false })
                        .then(function(response)
                        {
                            $log.info("Got the data from the server", response);
                            service.items = response.data;
                            service.populated = true;
                        })
                        .finally(function() {
                            service.populating = false;
                        });
                }, 3000);
            },
            
            delete: function(article) {
                if (article)
                    _.remove(service.items, function(item) {
                        return item.id == article.id;
                    })
            },
            
        };
      
        console.log("ArticlesService is ready.");
         
        return service;
    });