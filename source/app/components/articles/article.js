angular.module("NewsApp")
    .component("newsAppArticle", {  
        templateUrl: "/app/components/articles/article.html",
    
        bindings: {
          article: "="  
        },
    
        controller: function(ArticlesService) {
            // whatever defined in bindings above, is already available 
            // through this.***
            var ctrl = this;
            
            ctrl.delete = function() {
                ArticlesService.delete(ctrl.article);
            }
        }

    
    });


