// Declare a module
angular
    .module("NewsApp", ["ui.router"]);


// When AngularJS is wiring up things...
angular
    .module("NewsApp")
    .config(function($stateProvider, $urlRouterProvider) {
        console.log("Configuring...");
    
        $stateProvider
            .state("home", { url: "/", template: "<news-app-articles></news-app-articles>"})
            .state("login", { url: "/login", template: "<news-app-login></news-app-login>"})
            .state("about", { url: "/about", templateUrl: "/app/components/about/about.html"});
    
        $urlRouterProvider.otherwise("/");
    });


// When module is ready...
angular
    .module("NewsApp")
    .run(function() {
        console.log("Ready.");
    });

